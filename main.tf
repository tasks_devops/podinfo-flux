##### Requiered providers 
terraform {
  

  required_providers {
    kind = {
      source  = "tehcyx/kind"
      version = ">= 0.4"
    }
    flux = {
      source = "fluxcd/flux"
      version = ">= 1.2"
    }
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = ">= 16.10"
    }
    tls = {
      source  = "hashicorp/tls"
      version = ">= 4.0"
    }
  }
}

##### Resources
resource "kind_cluster" "this" {
  name     = var.cluster_name
  node_image = var.node_image
  kubeconfig_path = "~/.kube/config"
}
#Authentication for testing purposes, another method ishould be used for production
resource "tls_private_key" "flux" {
  algorithm   = "ECDSA"
  ecdsa_curve = "P256"
}
data "gitlab_project" "this" {
  path_with_namespace = "${var.gitlab_group}/${var.gitlab_project}"
}
resource "gitlab_deploy_key" "this" {
  project  = data.gitlab_project.this.id
  title    = "Flux"
  key      = tls_private_key.flux.public_key_openssh
  can_push = true
}

#Restricts Flux to this specific directory
resource "flux_bootstrap_git" "this" {
  depends_on = [gitlab_deploy_key.this]
  path = "./podinfo-flux"
  }