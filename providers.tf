provider "kind" {}

provider "gitlab" {
  token = var.gitlab_token
}

provider "flux" {
  kubernetes = {
     config_path = "~/.kube/config"
   }
  git = {
    url = "ssh://git@gitlab.com/${data.gitlab_project.this.path_with_namespace}.git"
    ssh = {
      username    = "git"
      private_key = tls_private_key.flux.private_key_pem
    }
  }
}