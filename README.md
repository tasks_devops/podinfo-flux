# Podinfo Deployment with Terraform and Flux

This project deploys Podinfo to a Kind cluster using Terraform and Helm via Flux.

## Prerequisites

* Terraform installed 
* Kind installed
* Git CLI installed
* FluxClI installed
* New Gitlab project
* Gitlab Personal Access Token (PAT)
* Optional - Lens



## Architecture

The project utilizes the following components:

* **Kind**: Creates a local Kubernetes cluster for development and testing purposes.
* **Flux**: GitOps tool for managing Kubernetes deployments.
* **GitLab**: Version control platform to store the Terraform configuration and Helm charts.
* **Podinfo**: Sample application that displays information about Kubernetes pods.

## Deployment Steps

1. Creation of a new Gitlab repository:
     ```
     You first need to push the yaml file that will be used by Flux in the directory to which you plan to bootstrap Flux
     
     ```


2. Configuration of the Terraform variables in `variables.tf`:
     ```
     cluster_name # Name of the Kind cluster
     node_image  # Image for the Kind cluster nodes
     gitlab_token  # GitLab PAT with access to the project (set as sensitive variable)
     gitlab_group  # GitLab group where the project resides
     gitlab_project # Name of the GitLab project containing the Terraform configuration and Helm charts
     ```

3. Initialize Terraform:

     ```bash
     terraform init
     ```

4. Apply the Terraform configuration:

     ```bash
     terraform apply
     ```

  It will ask for your PAT and when provided, it will create the Kind cluster with the name set in variables.tf, configure Flux with access to the GitLab project within the specifies directory in the resource, and deploy the Podinfo application using Helm via the specified repository in podinfo-helm.yaml and will watch for updates every 5 minutes.

## Accessing Podinfo

  Once the deployment is complete, you can access the Podinfo application using kubectl port-forward:

  ```bash
  kubectl port-forward -n default deployment/podinfo 8080:8080
  ```
  And then access http://localhost:8080 from your browser

  Alternatively you can use Lens to see the running deployments.