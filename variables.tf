variable "cluster_name" {
  type = string
  description = "K8s cluster name"
  default = "testk8s"
}

variable "node_image" {
  type = string
  description = "Image for the Cluster"
  default = "kindest/node:v1.27.1" 
}

variable "gitlab_token" {
  sensitive = true
  type = string
  description = "GitLab PAT"
}

variable "gitlab_group" {
  type = string
  description = "Gitlab group name"
  default = "tasks_devops" 
}

variable "gitlab_project" {
  type = string
  description = "Gitlab current project"
  default = "podinfo-flux"
}